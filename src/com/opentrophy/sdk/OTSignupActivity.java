package com.opentrophy.sdk;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.opentrophy.client.OTClientCallback;
import com.opentrophy.client.OTClientError;
import com.opentrophy.client.OTSigninInfo;
import com.opentrophy.client.OTSignupInfo;

public class OTSignupActivity extends Activity {

    private static final OTLog L = OTLog.getLog(OTSigninActivity.class);

    private Button closeBtn;
    private EditText usernameEdit;
    private EditText passwordEdit;
    private EditText passwordConfirmEdit;
    private Button signupBtn;
    private Button recoverBtn;
    private Button facebookBtn;
    private Button twitterBtn;
    private Button googleBtn;
    private Button signinBtn;

    private final View.OnClickListener closeClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            OTSignupActivity.this.finish();
        }
    };

    private final View.OnClickListener signupClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String username = usernameEdit.getText().toString();
            String password = passwordEdit.getText().toString();
            String passwordConfirm = passwordConfirmEdit.getText().toString();
            if (!password.equals(passwordConfirm)) {
                return;
            }

            OTSignupInfo signupInfo = new OTSignupInfo(new OTSigninInfo("local", username, password, true), null, true);
            OpenTrophy.getInstance().getClient().signup(signupInfo, new OTClientCallback() {

                @Override
                public void onSuccess(Object result) {
                    if (L.isDebugEnabled()) {
                        L.debug("signup ok: " + result);
                    }
                    OTSignupActivity.this.finish();
                }

                @Override
                public void onError(OTClientError error) {
                    if (L.isDebugEnabled()) {
                        L.debug("signup error: " + error);
                    }
                }
            });
        }
    };

    private final View.OnClickListener facebookClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // TODO: implement this!
        }
    };

    private final View.OnClickListener twitterClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // TODO: implement this!
        }
    };

    private final View.OnClickListener googleClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // TODO: implement this!
        }
    };

    private final View.OnClickListener signinClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(OTSignupActivity.this, OTSigninActivity.class);
            startActivity(intent);
        }
    };

    private final View.OnClickListener recoverClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(OTSignupActivity.this, OTRecoverActivity.class);
            startActivity(intent);
        }
    };


    private void updateView() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ot_signin);

        closeBtn = (Button) findViewById(R.id.ot_signup_close_btn);
        usernameEdit = (EditText) findViewById(R.id.ot_signup_username_edit);
        passwordEdit = (EditText) findViewById(R.id.ot_signup_password_edit);
        passwordConfirmEdit = (EditText) findViewById(R.id.ot_signup_password_confirm_edit);
        signupBtn = (Button) findViewById(R.id.ot_signup_signup_btn);
        facebookBtn = (Button) findViewById(R.id.ot_signup_facebook_btn);
        twitterBtn = (Button) findViewById(R.id.ot_signup_twitter_btn);
        googleBtn = (Button) findViewById(R.id.ot_signup_google_btn);
        signinBtn = (Button) findViewById(R.id.ot_signup_signin_btn);
        recoverBtn = (Button) findViewById(R.id.ot_signup_recover_btn);

        closeBtn.setOnClickListener(closeClickListener);
        signupBtn.setOnClickListener(signupClickListener);
        facebookBtn.setOnClickListener(facebookClickListener);
        twitterBtn.setOnClickListener(twitterClickListener);
        googleBtn.setOnClickListener(googleClickListener);
        signinBtn.setOnClickListener(signinClickListener);
        recoverBtn.setOnClickListener(recoverClickListener);

        updateView();
    }
}
