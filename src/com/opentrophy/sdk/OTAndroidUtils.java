package com.opentrophy.sdk;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.Settings;
import android.widget.Toast;
import com.opentrophy.client.OTUtils;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class OTAndroidUtils {

    private static final Bitmap FALLBACK_BITMAP = Bitmap.createBitmap(1, 1, Bitmap.Config.ALPHA_8);

    private static final String PREFERENCES_NAME = "OpenTrophy";

    public static Bitmap getBitmap(String src, Bitmap fallback) {
        InputStream in = null;
        try {
            URL url = new URL(src);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true);
            conn.connect();
            in = conn.getInputStream();
            return BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            return fallback;
        } finally {
            OTUtils.close(in);
        }
    }

    public static Bitmap getBitmap(String src) {
        return getBitmap(src, FALLBACK_BITMAP);
    }

    public static String getDeviceId(Context context) {
        // TODO: is this ok?
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, 2000).show();
    }

    public static void setPreference(Context context, String key, String value) {
        final SharedPreferences prefs = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        final SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putString(key, value);
        prefsEditor.commit();
    }

    public static String getPreferenceString(Context context, String key, String fallback) {
        final SharedPreferences prefs = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        return prefs.getString(key, fallback);
    }

    public static void removePreference(Context context, String key) {
        final SharedPreferences prefs = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        final SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.remove(key);
        prefsEditor.commit();
    }

}
