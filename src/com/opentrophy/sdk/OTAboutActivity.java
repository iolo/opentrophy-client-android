package com.opentrophy.sdk;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class OTAboutActivity extends Activity {

    private static final OTLog L = OTLog.getLog(OTAboutActivity.class);

    private Button closeBtn;
    private ImageView iconImg;
    private TextView titleText;
    private TextView subtitleText;

    private final View.OnClickListener closeClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            OTAboutActivity.this.finish();
        }
    };

    private void updateView() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ot_about);

        closeBtn = (Button) findViewById(R.id.ot_about_close_btn);
        iconImg = (ImageView) findViewById(R.id.ot_about_icon_img);
        titleText = (TextView) findViewById(R.id.ot_about_title_text);
        subtitleText = (TextView) findViewById(R.id.ot_about_subtitle_text);

        closeBtn.setOnClickListener(closeClickListener);

        updateView();
    }

}
