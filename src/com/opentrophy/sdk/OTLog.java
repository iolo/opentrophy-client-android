package com.opentrophy.sdk;

import android.util.Log;

public class OTLog implements org.apache.commons.logging.Log {

    private final String tag;

    private OTLog(Class klass) {
        String tag = klass.getName();
        if (tag.length() > 23) { // '23' is android magic number
            tag = tag.replace(klass.getPackage().getName(), "ot~");
            if (tag.length() > 23) {
                tag = tag.substring(tag.length() - 23);
            }
        }
        this.tag = tag;
    }

    @Override
    public boolean isDebugEnabled() {
        return true;//Log.isLoggable(tag, Log.DEBUG);
    }

    @Override
    public boolean isErrorEnabled() {
        return true;//Log.isLoggable(tag, Log.ERROR);
    }

    @Override
    public boolean isFatalEnabled() {
        return true;//Log.isLoggable(tag, Log.ASSERT);
    }

    @Override
    public boolean isInfoEnabled() {
        return true;//Log.isLoggable(tag, Log.INFO);
    }

    @Override
    public boolean isTraceEnabled() {
        return true;//Log.isLoggable(tag, Log.VERBOSE);
    }

    @Override
    public boolean isWarnEnabled() {
        return true;//Log.isLoggable(tag, Log.WARN);
    }

    @Override
    public void trace(Object o) {
        Log.v(tag, (String) o);
    }

    @Override
    public void trace(Object o, Throwable throwable) {
        Log.v(tag, (String) o, throwable);
    }

    @Override
    public void debug(Object o) {
        Log.d(tag, (String) o);
    }

    @Override
    public void debug(Object o, Throwable throwable) {
        Log.d(tag, (String) o, throwable);
    }

    @Override
    public void info(Object o) {
        Log.i(tag, (String) o);
    }

    @Override
    public void info(Object o, Throwable throwable) {
        Log.i(tag, (String) o, throwable);
    }

    @Override
    public void warn(Object o) {
        Log.w(tag, (String) o);
    }

    @Override
    public void warn(Object o, Throwable throwable) {
        Log.w(tag, (String) o, throwable);
    }

    @Override
    public void error(Object o) {
        Log.e(tag, (String) o);
    }

    @Override
    public void error(Object o, Throwable throwable) {
        Log.e(tag, (String) o, throwable);
    }

    @Override
    public void fatal(Object o) {
        Log.wtf(tag, (String) o);
    }

    @Override
    public void fatal(Object o, Throwable throwable) {
        Log.wtf(tag, (String) o, throwable);
    }

    //------------------------------------------

    public static OTLog getLog(Class klass) {
        return new OTLog(klass);
    }

}
