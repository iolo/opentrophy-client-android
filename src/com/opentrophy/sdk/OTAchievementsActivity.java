package com.opentrophy.sdk;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.opentrophy.client.OTAchievement;
import com.opentrophy.client.OTApp;
import com.opentrophy.client.OTClientCallback;
import com.opentrophy.client.OTClientError;

import java.util.List;

public class OTAchievementsActivity extends Activity {

    private static final OTLog L = OTLog.getLog(OTAchievement.class);

    private Button closeBtn;
    private ImageView iconImg;
    private TextView titleText;
    private TextView subtitleText;
    private ListView progressList;

    private final View.OnClickListener closeClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            OTAchievementsActivity.this.finish();
        }
    };

    private final OTClientCallback updateViewCallback = new OTClientCallback<List<OTAchievement>>() {
        @Override
        public void onSuccess(List<OTAchievement> achievements) {
            if (L.isDebugEnabled()) {
                L.debug("achievements ok: " + achievements);
            }
            progressList.setAdapter(new ProgressListAdapter(OTAchievementsActivity.this, achievements));
        }
        @Override
        public void onError(OTClientError error) {
            if (L.isDebugEnabled()) {
                L.debug("achievements error: " + error);
            }
            // TODO: more friendly error message
            titleText.setText("error:" + error);
        }

    };

    private void updateView() {
        final OTApp app = OpenTrophy.getInstance().getCurrentApp();
        if (app != null) {
            iconImg.setImageBitmap(OTAndroidUtils.getBitmap(app.getIcon()));
            titleText.setText(app.getTitle());
            subtitleText.setText(app.getSubtitle());
        }

        OpenTrophy.getInstance().getClient().getAppAchievements(null, updateViewCallback);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ot_achievements);

        closeBtn = (Button) findViewById(R.id.ot_achievements_close_btn);
        iconImg = (ImageView) findViewById(R.id.ot_achievements_icon_img);
        titleText = (TextView) findViewById(R.id.ot_achievements_title_text);
        subtitleText = (TextView) findViewById(R.id.ot_achievements_subtitle_text);
        progressList = (ListView) findViewById(R.id.ot_achievements_list);

        closeBtn.setOnClickListener(closeClickListener);

        updateView();
    }

    //---------------------------------------------------------------

    private class ProgressListAdapter extends ArrayAdapter<OTAchievement> {

        private class ViewHolder {
            ImageView iconImg;
            TextView progressText;
            TextView titleText;
            TextView subtitleText;
        }

        public ProgressListAdapter(Context context, List<OTAchievement> achievements) {
            super(context, R.layout.ot_achievements_item, achievements);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater layoutInflater = ((Activity) getContext()).getLayoutInflater();
                convertView = layoutInflater.inflate(R.layout.ot_achievements_item, null);
                viewHolder = new ViewHolder();
                viewHolder.iconImg = (ImageView) convertView.findViewById(R.id.ot_achievements_item_icon_img);
                viewHolder.progressText = (TextView) convertView.findViewById(R.id.ot_achievements_item_progress_text);
                viewHolder.titleText = (TextView) convertView.findViewById(R.id.ot_achievements_item_title_text);
                viewHolder.subtitleText = (TextView) convertView.findViewById(R.id.ot_achievements_item_subtitle_text);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            OTAchievement achievement = getItem(position);

            viewHolder.iconImg.setImageBitmap(OTAndroidUtils.getBitmap(achievement.getIcon()));
            //viewHolder.progressText.setText(achievement.getProgress() + "%");
            viewHolder.titleText.setText(achievement.getTitle());
            viewHolder.subtitleText.setText(achievement.getDescription());

            return convertView;
        }
    }

}
