package com.opentrophy.sdk;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.opentrophy.client.OTClientCallback;
import com.opentrophy.client.OTClientError;
import com.opentrophy.client.OTSigninInfo;

public class OTSigninActivity extends Activity {

    private static final OTLog L = OTLog.getLog(OTSigninActivity.class);

    private Button closeBtn;
    private EditText usernameEdit;
    private EditText passwordEdit;
    private Button signinBtn;
    private Button facebookBtn;
    private Button twitterBtn;
    private Button googleBtn;
    private Button signupBtn;
    private Button recoverBtn;

    private final View.OnClickListener closeClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            OTSigninActivity.this.finish();
        }
    };

    private final View.OnClickListener signinClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String username = usernameEdit.getText().toString();
            String password = passwordEdit.getText().toString();
            OTSigninInfo signinInfo = new OTSigninInfo("local", username, password, true);
            OpenTrophy.getInstance().getClient().signin(signinInfo, new OTClientCallback() {

                @Override
                public void onSuccess(Object result) {
                    if (L.isDebugEnabled()) {
                        L.debug("signin ok: " + result);
                    }
                    OTSigninActivity.this.finish();
                }

                @Override
                public void onError(OTClientError error) {
                    if (L.isDebugEnabled()) {
                        L.debug("signin error: " + error);
                    }
                }
            });
        }
    };

    private final View.OnClickListener facebookClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // TODO: implement this!
        }
    };

    private final View.OnClickListener twitterClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // TODO: implement this!
        }
    };

    private final View.OnClickListener googleClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // TODO: implement this!
        }
    };

    private final View.OnClickListener signupClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(OTSigninActivity.this, OTSignupActivity.class);
            startActivity(intent);
        }
    };

    private final View.OnClickListener recoverClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(OTSigninActivity.this, OTRecoverActivity.class);
            startActivity(intent);
        }
    };


    private void updateView() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ot_signin);

        closeBtn = (Button) findViewById(R.id.ot_signin_close_btn);
        usernameEdit = (EditText) findViewById(R.id.ot_signin_username_edit);
        passwordEdit = (EditText) findViewById(R.id.ot_signin_password_edit);
        signinBtn = (Button) findViewById(R.id.ot_signin_signin_btn);
        facebookBtn = (Button) findViewById(R.id.ot_signin_facebook_btn);
        twitterBtn = (Button) findViewById(R.id.ot_signin_twitter_btn);
        googleBtn = (Button) findViewById(R.id.ot_signin_google_btn);
        signupBtn = (Button) findViewById(R.id.ot_signin_signup_btn);
        recoverBtn = (Button) findViewById(R.id.ot_signin_recover_btn);

        closeBtn.setOnClickListener(closeClickListener);
        signinBtn.setOnClickListener(signinClickListener);
        facebookBtn.setOnClickListener(facebookClickListener);
        twitterBtn.setOnClickListener(twitterClickListener);
        googleBtn.setOnClickListener(googleClickListener);
        signupBtn.setOnClickListener(signupClickListener);
        recoverBtn.setOnClickListener(recoverClickListener);

        updateView();
    }

}
