package com.opentrophy.sdk;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.opentrophy.client.OTPlayer;
import com.opentrophy.client.OTUser;

public class OTAppsActivity extends Activity {

    private Button closeBtn;
    private ImageView iconImg;
    private TextView titleText;
    private TextView subtitleText;

    private final View.OnClickListener closeClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            OTAppsActivity.this.finish();
        }
    };

    private void updateView() {
        final OTPlayer player = OpenTrophy.getInstance().getCurrentPlayer();
        if (player != null) {
            iconImg.setImageBitmap(OTAndroidUtils.getBitmap(player.getIcon()));
            titleText.setText(player.getDisplayName());
            //subtitleText.setText(player.getDescription());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ot_apps);

        closeBtn = (Button) findViewById(R.id.ot_apps_close_btn);
        iconImg = (ImageView) findViewById(R.id.ot_apps_icon_img);
        titleText = (TextView) findViewById(R.id.ot_apps_title_text);
        subtitleText = (TextView) findViewById(R.id.ot_apps_subtitle_text);

        closeBtn.setOnClickListener(closeClickListener);

        updateView();
    }

}
