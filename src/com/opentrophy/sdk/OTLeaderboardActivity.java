package com.opentrophy.sdk;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.opentrophy.client.*;

import java.util.List;

public class OTLeaderboardActivity extends Activity {

    private static final OTLog L = OTLog.getLog(OTLeaderboardActivity.class);

    private Button closeBtn;
    private ImageView iconImg;
    private TextView titleText;
    private TextView subtitleText;
    private ListView scoreList;

    private final View.OnClickListener closeClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            OTLeaderboardActivity.this.finish();
        }
    };

    private final OTClientCallback<OTLeaderboardScores> updateViewCallback = new OTClientCallback<OTLeaderboardScores>() {
        @Override
        public void onError(OTClientError error) {
            if (L.isDebugEnabled()) {
                L.debug("trophy error: " + error);
            }
            // TODO: more friendly error message
            titleText.setText("error:" + error);
        }

        @Override
        public void onSuccess(OTLeaderboardScores leaderboardScores) {
            if (L.isDebugEnabled()) {
                L.debug("leaderboardScores ok: " + leaderboardScores);
            }

            iconImg.setImageBitmap(OTAndroidUtils.getBitmap(leaderboardScores.getLeaderboard().getIcon()));
            titleText.setText(leaderboardScores.getLeaderboard().getTitle());
            subtitleText.setText(leaderboardScores.getLeaderboard().getSubtitle());

            scoreList.setAdapter(new ScoreListAdapter(OTLeaderboardActivity.this, leaderboardScores));

        }
    };

    private void updateView() {
        String leaderboardId = getIntent().getStringExtra("leaderboardId");
        OpenTrophy.getInstance().getClient().getLeaderboardScores(leaderboardId, 0, 0, updateViewCallback);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ot_leaderboard);

        closeBtn = (Button) findViewById(R.id.ot_leaderboard_close_btn);
        iconImg = (ImageView) findViewById(R.id.ot_leaderboard_icon_img);
        titleText = (TextView) findViewById(R.id.ot_leaderboard_title_text);
        subtitleText = (TextView) findViewById(R.id.ot_leaderboard_subtitle_text);
        scoreList = (ListView) findViewById(R.id.ot_leaderboard_list);

        closeBtn.setOnClickListener(closeClickListener);

        updateView();
    }

    //---------------------------------------------------------------

    private class ScoreListAdapter extends ArrayAdapter<OTScore> {

        private final OTLeaderboardScores leaderboardScores;

        private class ViewHolder {
            TextView rankText;
            ImageView iconImg;
            TextView playerText;
            TextView scoreText;
            TextView unitText;
        }

        public ScoreListAdapter(Context context, OTLeaderboardScores leaderboardScores) {
            super(context, R.layout.ot_leaderboard_item, leaderboardScores.getScores());
            this.leaderboardScores = leaderboardScores;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater layoutInflater = ((Activity) getContext()).getLayoutInflater();
                convertView = layoutInflater.inflate(R.layout.ot_leaderboard_item, null);
                viewHolder = new ViewHolder();
                viewHolder.rankText = (TextView) convertView.findViewById(R.id.ot_leaderboard_item_rank_text);
                viewHolder.iconImg = (ImageView) convertView.findViewById(R.id.ot_leaderboard_item_icon_img);
                viewHolder.playerText = (TextView) convertView.findViewById(R.id.ot_leaderboard_item_player_text);
                viewHolder.scoreText = (TextView) convertView.findViewById(R.id.ot_leaderboard_item_score_text);
                viewHolder.unitText = (TextView) convertView.findViewById(R.id.ot_leaderboard_item_unit_text);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            OTScore score = getItem(position);

            viewHolder.rankText.setText(OTUtils.toString(position));
            viewHolder.iconImg.setImageBitmap(OTAndroidUtils.getBitmap(score.getPlayer().getIcon()));
            viewHolder.playerText.setText(score.getPlayer().getDisplayName());
            viewHolder.scoreText.setText(OTUtils.toString(score.getValue()));
            viewHolder.unitText.setText(leaderboardScores.getLeaderboard().getScoreFormat());

            return convertView;
        }
    }
}
