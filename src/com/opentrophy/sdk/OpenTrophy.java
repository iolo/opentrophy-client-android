package com.opentrophy.sdk;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.opentrophy.client.*;

public class OpenTrophy implements OTClientListener {

    private static final OTLog L = OTLog.getLog(OpenTrophy.class);

    private Context context;

    private OTClient client;

    private OTApp currentApp;

    private OTPlayer currentPlayer;

    public OpenTrophy() {
    }

    //
    // lifecycle methods
    //

    public void init(Context context, String key, String secret) {
        if (L.isInfoEnabled()) {
            L.info("OpenTrophy init...");
        }

        this.context = context;

        OTClientConfig config = new OTClientConfig(
                "519f771c2721470fd9000005",
                "test",
                "http://localhost:3000/auth/bearer",
                "http://localhost:3000/api/v1",
//                "http://192.168.11.31:3000/auth/bearer",
//                "http://192.168.11.31:3000/api/v1");
                OTAndroidUtils.getDeviceId(context),
                "android");

        client = new OTClient(config);

        client.addListener(this);

        currentPlayer = null;
        currentApp = null;

        restoreSession();
    }

    public void resume() {
        if (L.isInfoEnabled()) {
            L.info("OpenTrophy resume...");
        }
    }

    public void suspend() {
        if (L.isInfoEnabled()) {
            L.info("OpenTrophy suspend...");
        }
    }

    public void destroy() {
        if (L.isInfoEnabled()) {
            L.info("OpenTrophy destroy...");
        }
        context = null;
        client = null;
        currentPlayer = null;
        currentApp = null;
    }

    //
    //
    //

    public OTClient getClient() {
        return client;
    }

    public OTApp getCurrentApp() {
        return currentApp;
    }

    public OTPlayer getCurrentPlayer() {
        return currentPlayer;
    }

    public void showSignin() {
        Intent intent = new Intent(this.context, OTSigninActivity.class);
        context.startActivity(intent);
    }

    public void signout(OTClientCallback callback) {
        client.signout(callback);
    }

    public void showDashboard() {
        final Intent intent = new Intent(this.context, OTDashboardActivity.class);
        context.startActivity(intent);
    }

    public void showLeaderboards() {
        final Intent intent = new Intent(this.context, OTLeaderboardActivity.class);
        context.startActivity(intent);
    }

    public void showLeaderboard(int leaderboardId) {
        final Intent intent = new Intent(this.context, OTLeaderboardActivity.class);
        intent.putExtra("leaderboardId", leaderboardId);
        context.startActivity(intent);
    }

    public void showAchievements() {
        final Intent intent = new Intent(this.context, OTAchievementsActivity.class);
        context.startActivity(intent);
    }

    public void reportScore(String leaderboardId, int value, final OTClientCallback callback) {
        client.reportScore(leaderboardId, value, callback);
    }

    public void reportProgress(String achievementId, int value, final OTClientCallback callback) {
        client.reportProgress(achievementId, value, callback);
    }

    public void showProfile() {
        final Intent intent = new Intent(this.context, OTSettingsActivity.class);
        context.startActivity(intent);
    }

    public void showFriends() {
        final Intent intent = new Intent(this.context, OTFriendsActivity.class);
        context.startActivity(intent);
    }

    public void showApps() {
        final Intent intent = new Intent(this.context, OTAppsActivity.class);
        context.startActivity(intent);
    }

    public void setAttribute(String key, Object value, final OTClientCallback callback) {
//        client.setAttribute(key, value, callback);
    }

    public void getAttribute(String key, final OTClientCallback callback) {
//        client.getAttribute(key, callback);
    }

    public void removeAttribute(String key, final OTClientCallback callback) {
//        client.removeAttribute(key, callback);
    }

    public void removeAllAttributes(final OTClientCallback callback) {
//        client.removeAllAttributes(callback);
    }

    //---------------------------------------------------------------

    @Override
    public void onConnect(OTApp app) {
        currentApp = app;
//        OTAndroidUtils.setPreference(context, "accessToken", client.getAccessToken());
    }

    @Override
    public void onDisconnect() {
        //OTAndroidUtils.removePreference(context, "accessToken");
    }

    @Override
    public void onSignin(OTPlayer player) {
        currentPlayer = player;
        String msg = "OpenTrophy sign: player=" + currentPlayer.getDisplayName() + ",app=" + currentApp.getTitle();
        OTAndroidUtils.showToast(context, msg);
    }

    @Override
    public void onSignout() {
        currentPlayer = null;
        String msg = "OpenTrophy signout!";
        OTAndroidUtils.showToast(context, msg);
    }

    //---------------------------------------------------------------

    private void restoreSession() {
        final String accessToken = OTAndroidUtils.getPreferenceString(context, "accessToken", "");
        if (!TextUtils.isEmpty(accessToken)) {
            if (L.isDebugEnabled()) {
                L.debug("try to restore local player.... accessToken=" + accessToken);
            }
            client.connect(null);
        }
    }

    //---------------------------------------------------------------

    private static volatile OpenTrophy instance = null;

    public static OpenTrophy getInstance() {
        if (instance == null) {
            synchronized (OpenTrophy.class) {
                if (instance == null) {
                    instance = new OpenTrophy();
                }
            }
        }
        return instance;
    }

}
