package com.opentrophy.sdk;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.opentrophy.client.OTApp;
import com.opentrophy.client.OTPlayer;
import com.opentrophy.client.OTUser;

public class OTDashboardActivity extends Activity {

    private static final String LOG_TAG = "ot_dashboard";

    private ImageView iconImg;
    private TextView titleText;
    private TextView subtitleText;
    private Button closeBtn;

    private ImageView profileIconImg;
    private TextView profileTitleText;
    private TextView profileSubtitleText;
    private Button profileBtn;

    private Button leaderboardsBtn;
    private Button achievementsBtn;
    private Button friendsBtn;
    private Button itemsBtn;

    private final View.OnClickListener closeClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            OTDashboardActivity.this.finish();
        }
    };

    private final View.OnClickListener profileClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            OpenTrophy.getInstance().showProfile();
        }
    };

    private final View.OnClickListener leaderboardsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            OpenTrophy.getInstance().showLeaderboards();
        }
    };

    private final View.OnClickListener achievementsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            OpenTrophy.getInstance().showAchievements();
        }
    };

    private final View.OnClickListener friendsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            OpenTrophy.getInstance().showFriends();
        }
    };

    private final View.OnClickListener itemsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            OpenTrophy.getInstance().showApps();
        }
    };

    private void updateView() {
        final OTApp app = OpenTrophy.getInstance().getCurrentApp();
        if (app != null) {
            iconImg.setImageBitmap(OTAndroidUtils.getBitmap(app.getIcon()));
            titleText.setText(app.getTitle());
            subtitleText.setText(app.getSubtitle());
        }

        final OTPlayer player = OpenTrophy.getInstance().getCurrentPlayer();
        if (player != null) {
            profileIconImg.setImageBitmap(OTAndroidUtils.getBitmap(player.getIcon()));
            profileTitleText.setText(player.getDisplayName());
            //profileSubtitleText.setText(player.getDescription());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ot_dashboard);

        iconImg = (ImageView) findViewById(R.id.ot_dashboard_icon_img);
        titleText = (TextView) findViewById(R.id.ot_dashboard_title_text);
        subtitleText = (TextView) findViewById(R.id.ot_dashboard_subtitle_text);
        closeBtn = (Button) findViewById(R.id.ot_dashboard_close_btn);
        profileIconImg = (ImageView) findViewById(R.id.ot_dashboard_profile_icon_img);
        profileTitleText = (TextView) findViewById(R.id.ot_dashboard_profile_title_text);
        profileSubtitleText = (TextView) findViewById(R.id.ot_dashboard_profile_subtitle_text);
        profileBtn = (Button) findViewById(R.id.ot_dashboard_profile_btn);
        leaderboardsBtn = (Button) findViewById(R.id.ot_dashboard_leaderboards_btn);
        achievementsBtn = (Button) findViewById(R.id.ot_dashboard_achievements_btn);
        friendsBtn = (Button) findViewById(R.id.ot_dashboard_friends_btn);
        itemsBtn = (Button) findViewById(R.id.ot_dashboard_items_btn);

        closeBtn.setOnClickListener(closeClickListener);
        profileBtn.setOnClickListener(profileClickListener);
        leaderboardsBtn.setOnClickListener(leaderboardsClickListener);
        achievementsBtn.setOnClickListener(achievementsClickListener);
        friendsBtn.setOnClickListener(friendsClickListener);
        itemsBtn.setOnClickListener(itemsClickListener);

        updateView();
    }

}
